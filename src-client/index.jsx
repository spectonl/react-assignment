import React from 'react';
import ReactDOM from 'react-dom';

import {Reservations} from './components/Reservations';
import './styles/index.less';

ReactDOM.render(
    <Reservations customerid='1337' />,
    document.getElementById('app')
);
