import React, {PropTypes} from 'react';

export class ItemList extends React.Component {
    constructor() {
        super();
        this.state = this.getInitialState();
    }

    getInitialState() {
        return {};
    }

    render() {
        const items = this.props.items;

        return (
            <ul>
                {items.map((item) => <li key={item.id}>{item.product.name}</li>)}
            </ul>
        );
    }
}

ItemList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object)
};
