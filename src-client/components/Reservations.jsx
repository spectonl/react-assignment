import React, {PropTypes} from 'react';
import io from 'socket.io-client';
import 'promise';
import {ItemList} from './reservations/ItemList';

import './styles/Reservations.less';

const IO_EMIT_GET_DATA = 'getReservationData',
    IO_ON_GET_DATA = 'reservationData';

export class Reservations extends React.Component {
    constructor() {
        super();
        this.state = this.getInitialState();

        this.socket = io();
    }

    getInitialState() {
        return {customer: {}, assignee: {}, items: [], orderMetaData: {}};
    }
    componentWillMount() {
        // TODO: enable loading indication
        this.socket.emit(IO_EMIT_GET_DATA, {
            customerid: this.props.customerid
        });
        this.socket.on(IO_ON_GET_DATA, (reservationDataResponse) => {
            // TODO: disable loading indication
            const data = reservationDataResponse.data;

            this.setState({
                customer: data.customer,
                assignee: data.assignee,
                items: data.items,
                orderMetaData: {
                    placedAt: data.placedAt,
                    status: data.status
                }
            });

            console.log(data);
        });
    }

    render() {
        return (
            <div className='reservations'>
                <div className='header'>
                    <h1>{this.state.customer.name}</h1>
                    <h2>{this.state.customer.id}</h2>
                </div>
                <div className='primaryOrderData'>
                    <dl>
                        <dt>{'Aangevraagd om'}</dt>
                        <dd>{this.state.orderMetaData.placedAt}</dd>
                        <dt>{'Email'}</dt>
                        <dd>{this.state.customer.email}</dd>
                        <dt>{'Telefoon'}</dt>
                        <dd>{this.state.customer.phoneNumber}</dd>
                    </dl>
                </div>
                <div className='secondaryOrderData'>
                    <dl>
                        <dt>{'Status'}</dt>
                        <dd>{this.state.orderMetaData.status}</dd>
                        <dt>{'Door'}</dt>
                        <dd>{this.state.assignee.name}</dd>
                        <dt>{'Om'}</dt>
                        <dd>{this.state.orderMetaData.placedAt}</dd>
                    </dl>
                </div>
                <ItemList items={this.state.items} />
            </div>
        );
    }
}

Reservations.propTypes = {
    customerid: PropTypes.Number
};
