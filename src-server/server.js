const express = require('express'),
    http = require('http'),
    io = require('socket.io'),
    path = require('path'),
    fs = require('fs');

const PORT = 3000,
    app = express(),
    server = http.createServer(app),
    ioServer = io(server),
    reservationFilePath = path.join(__dirname, 'data', 'reservation-request.json'),
    reservationData = readJsonFileSync(reservationFilePath);

// Routing
app.use(express.static(path.join(__dirname, '/../dist-client')));

server.listen(PORT, () => {
    console.log('Server listening at port %d', PORT);
});

ioServer.on('connection', (socket) => {
    console.log('Socket connected');

    socket.on('getReservationData', () => {
        socket.emit('reservationData', reservationData);
    });

    socket.on('serverName', () => {
        socket.emit('serverName', {
            name: 'expressjs'
        });
        console.log('Socket received serverName');
    });
});


function readJsonFileSync(filepath, encoding) {
    const fileEncoding = encoding || 'utf8';
    const file = fs.readFileSync(filepath, fileEncoding);

    return JSON.parse(file);
}
